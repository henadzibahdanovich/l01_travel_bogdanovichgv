//
//  CurrencyService.swift
//  TravelApp
//
//  Created by Gena Bogdanovich on 22.11.2018.
//  Copyright © 2018 Gena Bogdanovich. All rights reserved.
//

import Foundation
import Alamofire

class CurrencyService {
    private enum Constants{
        static let baseURL = "http://data.fixer.io/api/"
        static let accessKey = "API_KEY"

    }
    private enum EndPoints{
        static let latest = "/latest"


    }

    func getCurrency(for currencies: [Currency], completion: (() -> Void)?) {
        let urlString = Constants.baseURL + EndPoints.latest
        let url = URL.init(string: urlString)!
        let currenciesString = currencies.map({$0.rawValue}).joined(separator: ",")
        let parameters = ["access_key": Constants.accessKey, "symbols" : "EUR,USD,RUB"]
        Alamofire.request(url, method: .get, parameters: parameters).responseJSON { (response) in
            
        }
    }
    
}



enum Currency: String {
    case euro = "EUR"
    case dollar = "USD"
    case ruble = "RUB"
    func sign() -> String {
        switch self {
        case .euro:
            return "€"
        case .dollar:
            return "$"
        case .ruble:
            return "₽"
        }
    }
}
