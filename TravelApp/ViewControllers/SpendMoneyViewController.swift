//
//  SpendMoneyViewController.swift
//  TravelApp
//
//  Created by Gena Bogdanovich on 16.10.2018.
//  Copyright © 2018 Gena Bogdanovich. All rights reserved.
//

import UIKit


class SpendMoneyViewController: UIViewController {
    
    weak var delegate: TravelCreatorViewController?
    
    @IBOutlet weak var spendetMoneyTextFieldOutlet: UITextField!
    @IBOutlet weak var scrollViewSpendMoneyOutlet: UIScrollView!
    @IBOutlet weak var readyOutletButton: UIButton!
    @IBOutlet weak var valutSegmentedController: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spendetMoneyTextFieldOutlet.setLeftPaddingPoints(10)
        buttonStyle()
    }
    
    @IBAction func readyButtonClicked(_ sender: UIButton) {
        let result = spentMoneyNumber()
        delegate?.spendMoneyFinished(withResult: result)
        self.dismiss(animated: true, completion: nil)
    }
    
    func spentMoneyNumber() -> String{
        var resultNumber = ""
        if let sum = spendetMoneyTextFieldOutlet.text{
            resultNumber = sum
        }
        return resultNumber + valueOfSegmentedController(result: valutSegmentedController.selectedSegmentIndex)
    }
    
    func buttonStyle() {
        readyOutletButton.layer.cornerRadius = 15
    }

    func valueOfSegmentedController(result: Int) -> String {
        switch result {
        case 0:
            return "$"
        case 1:
            return "€"
        default:
            return "₽"
        }

    }
    
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
}
