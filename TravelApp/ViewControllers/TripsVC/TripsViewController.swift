//
//  TripsViewController.swift
//  TravelApp
//
//  Created by Gena Bogdanovich on 23.11.2018.
//  Copyright © 2018 Gena Bogdanovich. All rights reserved.
//

import UIKit
import EzPopup
import CoreData

class TripsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, PopUpViewControllerDelegate{
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CoreDataManager.shared.trips.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tripsCell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! TripsCell
        let trip = CoreDataManager.shared.trips[indexPath.row]
        
        tripsCell.countryNameLabel.text = trip.name
        tripsCell.tripsDiscriptionLabel.text = trip.tripDiscription
        
        return tripsCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let placeVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "placeVCID") as! PlacesViewController)
        placeVC.trip = CoreDataManager.shared.trips[indexPath.row]

        navigationController?.pushViewController(placeVC, animated: true)
    }
  
    
    @IBOutlet weak var popUpSuperView: UIView!
    
    @IBOutlet weak var emptyTripsLabel: UILabel!
    
    @IBOutlet weak var tripsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tripsTableView.dataSource = self
        self.tripsTableView.delegate = self
        
        tripsTableView.tableFooterView = UIView()

        navigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
    }
    
    @IBAction func createTripButtonClicked(_ sender: Any) {
        let viewController = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popUpVC") as! PopUpViewController)
        viewController.delegate = self
        let popupVC = PopupViewController(contentController: viewController, popupWidth: 300, popupHeight: 200)
        popupVC.cornerRadius = 20
        present(popupVC, animated: true)
        
    }
    
    func navigationBar(){
        let nav = self.navigationController?.navigationBar
        self.title = "Путешествия"
        nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
    }
  
    func reloadTableView() {
        tripsTableView.reloadData()
    }
    
    
}


