//
//  TripsCell.swift
//  TravelApp
//
//  Created by Gena Bogdanovich on 23.11.2018.
//  Copyright © 2018 Gena Bogdanovich. All rights reserved.
//

import UIKit

class TripsCell: UITableViewCell {
    
    @IBOutlet weak var tripsBgView: UIView!
    
    @IBOutlet weak var countryNameLabel: UILabel!
    
    @IBOutlet weak var tripsDiscriptionLabel: UILabel!
    
    override func awakeFromNib() { // вызывается когда загружена ячейка
        super.awakeFromNib()
        
        tripsBgView.layer.cornerRadius = 15

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    
}

}
