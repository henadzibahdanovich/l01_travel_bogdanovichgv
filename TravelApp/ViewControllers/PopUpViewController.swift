//
//  PopUpViewController.swift
//  TravelApp
//
//  Created by Gena Bogdanovich on 30.11.2018.
//  Copyright © 2018 Gena Bogdanovich. All rights reserved.
//

import UIKit
import CoreData

protocol PopUpViewControllerDelegate {
    func reloadTableView()
}

class PopUpViewController: UIViewController {
    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var popUpViewTripLabel: UILabel!
    
    @IBOutlet weak var insertTripNameTextField: UITextField!
    
    @IBOutlet weak var insertDiscriptionTextField: UITextField!
    
    @IBOutlet weak var readyButton: UIButton!
    
        var delegate : PopUpViewControllerDelegate?
    
    var createTripClicked : (()-> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        readyButton.layer.cornerRadius = 15
        
    }
    
    
    
    @IBAction public func createPopUpTripButtonClicked(_ sender: Any) {
        if let entityDescription = NSEntityDescription.entity(forEntityName: "CDTrip", in: CoreDataManager.shared.managedObjectContext) {
            let trip = CDTrip.init(entity: entityDescription, insertInto: CoreDataManager.shared.managedObjectContext)
            
            guard let tripName = insertTripNameTextField.text, !tripName.isEmpty,
                let tripDiscription = insertDiscriptionTextField.text, !tripDiscription.isEmpty else {
                    showAlert(message: "Проверьте введенные данные!")
                    return
            }
            trip.name = String(tripName)
            trip.tripDiscription = String(tripDiscription)
            
            CoreDataManager.shared.saveContext()
            self.dismiss(animated: true, completion: nil)
//          createTripClicked?(trip)
          delegate?.reloadTableView()
        }
    }
    
    
    func showAlert(message: String) {
        let alert = UIAlertController.init(title: "Ошибка", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Ок", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
