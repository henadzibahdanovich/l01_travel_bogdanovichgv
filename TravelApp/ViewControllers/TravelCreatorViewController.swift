//
//  PlaceViewController.swift
//  TravelApp
//
//  Created by Gena Bogdanovich on 17.10.2018.
//  Copyright © 2018 Gena Bogdanovich. All rights reserved.
//
//для bitbucket soutce tree

import UIKit
import CoreData

protocol PlaceViewControllerDelegate {
    func placeViewControllerReloadData()
}

class TravelCreatorViewController: UIViewController  {
    
    var trip : CDTrip!
    var delegate2: PlaceViewControllerDelegate?
    
    //MARK: - OUTLETS
    
    @IBOutlet weak var placeNameTextFieldOutlet: UITextField!
    
    @IBOutlet weak var raitingTextFieldOutlet: UITextField!
    
    @IBOutlet weak var coordinateLabel: UILabel!
    
    @IBOutlet weak var spendMoneyNomberOutlet: UILabel!
    
    @IBOutlet weak var segmentedTransportChoseOutlet: UISegmentedControl!
    
    @IBOutlet weak var textViewOutlet: UITextView!
    
    @IBOutlet weak var scrollViewOutlet: UIScrollView!
    
    @IBOutlet weak var stepperOutlet: UIStepper!

    
    //MARK: - ACTIONS
    
    @IBAction func cancelButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func stapperAction(_ sender: UIStepper) {
        raitingTextFieldOutlet.text = "\(Int(sender.value))"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "MapVCIdentifire" {
            let mapVC = segue.destination as? MapViewController
        } else if segue.identifier == "spendMoneyVCIdentifier" {
            let spendMoneyVC = segue.destination as? SpendMoneyViewController
            spendMoneyVC?.delegate = self
        }
        
    }

    func spendMoneyFinished(withResult result: String) {
        print("result:\(result)")
        spendMoneyNomberOutlet.text = result
    }
    
    
    
    @IBAction func mapViewCloseButtonClicked(_ segue: UIStoryboardSegue) {
        if let mapViewController = segue.source as? MapViewController{
            if let selectedCoordinate = mapViewController.mapView.selectedAnnotations.first {
                self.coordinateLabel.text = "lat:(\(selectedCoordinate.coordinate.latitude)), long:(\(selectedCoordinate.coordinate.longitude))"
            }
        }
    }
    
    
    @IBAction func saveButtonAction(_ sender: Any) {
        if let entityDescription = NSEntityDescription.entity(forEntityName: "CDPlace", in: CoreDataManager.shared.managedObjectContext) {
            let place = CDPlace.init(entity: entityDescription, insertInto: CoreDataManager.shared.managedObjectContext)
            
            guard let travelPlace = placeNameTextFieldOutlet.text , !travelPlace.isEmpty, let travelRaiting = raitingTextFieldOutlet.text , !travelRaiting.isEmpty, let travelSpendMoney = spendMoneyNomberOutlet.text , !travelSpendMoney.isEmpty, let travelReview = textViewOutlet.text , !travelReview.isEmpty else {
                    showAlert()
                    return
            }
            
            let travelTransport = transportSegmentedChosen(result: segmentedTransportChoseOutlet.selectedSegmentIndex)
            place.transport = travelTransport
            place.placeName = travelPlace
            if let raiting = Int(travelRaiting){
                place.rating = Int32(raiting)
            }
            place.spendMoney = travelSpendMoney
            place.placeDiscription = travelReview
            trip.addToPlaces(place)
            CoreDataManager.shared.saveContext()
        
            self.navigationController?.popViewController(animated: true)
            delegate2?.placeViewControllerReloadData()
        }
    }
    
    func transportSegmentedChosen(result: Int) -> String {
        switch result {
        case 0:
            return "🛩"
        case 1:
            return "🚂"
        default:
            return "🚙"
        }
    }
    
    func showAlert() {
        let alert = UIAlertController.init(title: "Ошибка", message: "Введите все данные о месте.", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Закрыть", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
}
