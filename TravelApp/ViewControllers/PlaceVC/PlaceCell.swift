//
//  TripCell.swift
//  TravelApp
//
//  Created by Gena Bogdanovich on 01.11.2018.
//  Copyright © 2018 Gena Bogdanovich. All rights reserved.
//

import UIKit

class PlaceCell: UITableViewCell {

    //MARK: - OUTLETS
    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var placeNameLabel: UILabel!
    
    @IBOutlet weak var discriptionLabel: UILabel!
    
    @IBOutlet weak var spendMoneyLabel: UILabel!
    
    @IBOutlet weak var transportImage: UIImageView!
    
    @IBOutlet var starsImageOutletCollection: [UIImageView]!
    
    override func awakeFromNib() { // вызывается когда загружена ячейка
        super.awakeFromNib()

        bgView.layer.cornerRadius = 15
        
    }

    //MARK: - FUNCTIONS
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        
    }

}
