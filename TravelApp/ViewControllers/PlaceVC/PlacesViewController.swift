//
//  PlacesViewController.swift
//  TravelApp
//
//  Created by Gena Bogdanovich on 23.10.2018.
//  Copyright © 2018 Gena Bogdanovich. All rights reserved.
//

import UIKit
import CoreData


class PlacesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, PlaceViewControllerDelegate{
  
    // MARK: - PROPERTIES
    
    var trip: CDTrip?
    
    // MARK: - OUTLETS
    
    @IBOutlet weak var emptyLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!

    
    // MARK: - FUNCTIONS
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let trip = trip, let places = trip.places {
            return places.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! PlaceCell
        
        guard let trip = trip, let placesSet = trip.places else {
            return PlaceCell()
        }
        let places = Array(placesSet) as! [CDPlace]
        let travel = places[indexPath.row]
        
        cell.placeNameLabel.text = travel.placeName
        cell.discriptionLabel.text = travel.placeDiscription
        cell.spendMoneyLabel.text = "\(travel.spendMoney)"
        
        if travel.transport == "🛩" {
            cell.transportImage.image = UIImage(named: "icon_plane")
        } else if travel.transport == "🚂"{
            cell.transportImage.image = UIImage(named: "icon_train")
        } else if travel.transport == "🚙"{
            cell.transportImage.image = UIImage(named: "icon_car")
        }
    
        for (index, imageView) in cell.starsImageOutletCollection.enumerated() {
            imageView.isHighlighted = travel.rating >= index
        }
        return cell
    }
    
    func placeViewControllerReloadData() {
        tableView.reloadData()
    }
    
    // MARK: - LYFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        
        barItems()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        self.tableView.reloadData()
    }
    
    func barItems() {

        let nav = self.navigationController?.navigationBar
        self.title = trip?.name
        nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
    }

    @IBAction func addPlaceButtonClicked(_ sender: Any) {
        let tripCreateVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tripCreateVC") as! TravelCreatorViewController)
        tripCreateVC.delegate2 = self
        tripCreateVC.trip = trip!
        navigationController?.pushViewController(tripCreateVC, animated: true)
    }
    
    
}






































/*
 override func viewWillAppear(_ animated: Bool) { // экран еще не показан и не сверстан
 super.viewWillAppear(animated)
 
 }
 
 override func viewDidAppear(_ animated: Bool) { // экран уже показан и сверстан
 super.viewDidAppear(animated)
 }
 
 
 override func viewWillDisappear(_ animated: Bool) { // срабатывает за доли секунды до того как экран срячется
 super.viewWillDisappear(animated)
 
 }
 
 override func viewDidDisappear(_ animated: Bool) { //срабатывает когда экран закроется
 super.viewDidDisappear(animated)
 }
 
 override func viewWillLayoutSubviews() { // срабатывает когда констрэйнты устанавливаются
 super.viewWillLayoutSubviews()
 }
 
 override func viewDidLayoutSubviews() { //срабатывает когда констрэйнты установлены
 super.viewDidLayoutSubviews()
 }
 
 override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {//срабатывает при смене ориентации экрана
 super.viewWillTransition(to: size, with: coordinator)
 }
 
 override func didReceiveMemoryWarning() { //сообщит если у вас что-то не так с памятью
 super.didReceiveMemoryWarning()
 
 }
 
 deinit { // срабатывает когда вьюконтроллер удалился из памяти (можно проверить отчистилась память или нет)
 
 }
 */



