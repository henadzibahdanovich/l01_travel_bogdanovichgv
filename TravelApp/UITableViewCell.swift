//
//  UITableViewCell.swift
//  TravelApp
//
//  Created by Gena Bogdanovich on 01.11.2018.
//  Copyright © 2018 Gena Bogdanovich. All rights reserved.
//

import UIKit

class UITableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
